require_relative "mod_sentiment.rb"
include Sentiment

puts "All top level comments with selftext"
Sentiment::Reddit.new("reddit").worker

puts "Daily BTC >> daily.json"
Sentiment::Reddit.new("reddit").get_thread("bitcoinmarkets")

puts "Daily EthTrader >> daily.json"
Sentiment::Reddit.new("reddit").get_thread("ethtrader")
