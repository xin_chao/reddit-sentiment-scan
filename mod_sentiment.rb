module Sentiment

  extend self

  require "net/http"
  require "uri"
  require "json"
  require "yaml"
  require "mongo"
  require "firebase"
  require "net/https"

  private
  def db_con
    begin
      cdata = config
      client = Mongo::Client.new([ cdata[0] ],user: cdata[1],password: cdata[2],:database => cdata[3])
    rescue
    	puts "Error connecting to database."
    end
   return client
  end

  private
  def config
      conf = YAML::load_file(  File.join(__dir__, 'config.yaml')  )
      cdata = [conf['server'],conf['user'],conf['pass'],conf['db']]
    return cdata
  end

  class Crawler

    attr_reader :tocall, :baseconf, :firebase
    attr_accessor :basepwd, :dbdump, :confarea

    def initialize baseconf
      @confarea = YAML::load_file( File.join(__dir__, 'config.yaml') )[baseconf]
      @firebase = YAML::load_file( File.join(__dir__, 'config.yaml') )[baseconf]['firebase']
      @firesecret = YAML::load_file( File.join(__dir__, 'config.yaml') )[baseconf]['firesecret']
      @basepwd = YAML::load_file( File.join(__dir__, 'config.yaml') )['basepwd']
      @dbdump = YAML::load_file( File.join(__dir__, 'config.yaml') )['dbdump']
    end

    def worker
     "$#{get_response}"
     "$#{get_mongo}"
    end


    def get_response
      raise NotImplementedError, "get_response method not implemented in #{self.class}"
    end
    def post_firebase item
      raise NotImplementedError, "post_firebase method not implemented in #{self.class}"
    end
    def put_mongo item, col
      raise NotImplementedError, "put_mongo method not implemented in #{self.class}"
    end
    def get_mongo
      raise NotImplementedError, "get_mongo method not implemented in #{self.class}"
    end

  end

  class Reddit < Crawler

    def get_thread subname

      puts "Subreddit: #{subname}"
      token = auth
      kws = @confarea['kw']
      subs = @confarea['slug']
      pubdir = @dbdump

      call = "https://oauth.reddit.com/r/#{subname}.json?sort=new&t=day"
      url = URI.parse( call )
      headers = {"Authorization" => token, "User-Agent" => "linux:sentiment v0.1.1 by /u/_xin_chao" }
       https = Net::HTTP.new(url.host,url.port)
       https.use_ssl = true
       https.verify_mode = OpenSSL::SSL::VERIFY_NONE
       p https
      req = Net::HTTP::Get.new(url.request_uri, headers)
      res = https.request(req)

      data = JSON.parse( res.body )

      to_search = "discussion"
      today = "#{Time.new.day}"
      thismonth = "#{Date.today.strftime("%B").downcase}"
       kids = data['data']['children']
       new_call = "unset"

       kids.each do |child|
        title = child['data']['title'].downcase
          if title.include? to_search
            if (title.include? today) && (title.include? thismonth)
            new_call = "https://oauth.reddit.com" + child['data']['permalink'] + ".json?sort=new&t=day"
            end
          end
       end

       if new_call=="unset" then p "Thread not found, aborting."; exit end

       puts "Today is day #{today} of month #{thismonth}, pulling daily thread: #{new_call}"

       url = URI.parse( new_call )
       headers = {"Authorization" => token, "User-Agent" => "linux:sentiment v0.1.1 by /u/_xin_chao" }
        https = Net::HTTP.new(url.host,url.port)
        https.use_ssl = true
        https.verify_mode = OpenSSL::SSL::VERIFY_NONE
        puts "Daily: Reddit response #{https}"
       req = Net::HTTP::Get.new(url.request_uri, headers)
       res = https.request(req)
       data = JSON.parse( res.body )

       kids = data[1]['data']['children']
       file = "#{pubdir}" + "#{subname}-daily.json"
       items = []
       cl = db_con
       col = cl[:redditdaily0]
       p file

       ### !!!!!
       ## Dailies change at 6AM CET, needs to be synched with server time

       kids.each do |child|

             item = {
              :name => child['data']['name'],
              :id => child['data']['id'],
              :parent_id => child['data']['parent_id'],
              :body => child['data']['body'],
              :body_html => child['data']['body_html'],
              :author => child['data']['author'],
              :score => child['data']['score'],
              :keyword => "#{Date.today}",
              :subreddit => "/r/#{subname}"
            }
            items.push(item)
            # send to firebase
            #self.post_firebase( item )
            # save in db
            duplicates = col.find({ name: child['data']['name'] }).limit(1)
            next if duplicates.count != 0
            self.put_mongo( item, col )
       end
      # items.reverse!
       unless items==[]
         all = { :query => "Daily Discussion /r/#{subname} #{Date.today}", :data => items }
         jf = open( file, 'w+')
         jf.write( all.to_json )
         jf.close
       end
    end
    def get_response

      token = auth
      kws = @confarea['kw']
      subs = @confarea['slug']
      pubdir = @dbdump

      subs.each do |s|
         call = "https://oauth.reddit.com/r/" + s + ".json?sort=new&t=day"
         url = URI.parse( call )
         headers = {"Authorization" => token, "User-Agent" => "linux:sentiment v0.1.1 by /u/_xin_chao" }
          https = Net::HTTP.new(url.host,url.port)
           https.use_ssl = true
            https.verify_mode = OpenSSL::SSL::VERIFY_NONE
            p https
         req = Net::HTTP::Get.new(url.request_uri, headers)
         res = https.request(req)

         data = JSON.parse( res.body )

         cl = db_con
         col = cl[:reddit2]

         kids = data['data']['children']
           kids.each do |child|
               kws.each do |k|

                 file = pubdir + s + "_" + k + ".json"

                 selftext = child['data']['selftext'].downcase

                     if selftext.include? k

                           item = {
                            :title => child['data']['title'],
                            :permalink => child['data']['permalink'],
                            :selftext => child['data']['selftext'],
                            :author => child['data']['author'],
                            :score => child['data']['score'],
                            :utc => child['data']['created_utc'],
                            :keyword => k,
                            :subreddit => s
                          }
                          # temporary?
                          p "Storing to file #{file}"
                          jf = open( file, 'w+')
                          jf.write( item.to_json )
                          jf.close
                          #id = child['data']['created_utc']
                          duplicates = col.find({ title: child['data']['title'] }).limit(1)
                          next if duplicates.count != 0
                          # send to firebase
                          self.post_firebase( item )
                          # save in db
                          self.put_mongo( item, col )


                    end

               end
           end
      end
    end
      def post_firebase item
         firebase = Firebase::Client.new( @firebase, @firesecret )
          hl = "comment_" + Time.new.month.to_s
         response = firebase.push( hl, item )
          suc = response.success?
          p "Firebase success #{suc}"
      end
      def put_mongo item, col
          if col.count < 1 then col.create end
          result = col.insert_one( item )
      end
    def get_mongo
     cl = db_con
     col = cl[:reddit2]

     kws = @confarea['kw']
         kws.each do |kw|
          documents = col.find({ keyword: kw }) #.skip(col.count() - 100)
          outer = []
           documents.each do |document|
             inner = {
               :title => document["title"],
               :author => document["author"],
               :permalink => document["permalink"],
               :selftext => document["selftext"]
               }
             outer.push( inner )
           end
          outer.reverse!
          if outer.count>10 then outer = outer[0..9] end
          all = { :query => "#{kw} - #{Date.today} - newest first", :data => outer }
          file = @dbdump + kw.to_s + ".json"
           jf = open( file, 'w+')
            jf.write( all.to_json )
             jf.close
         end
      # prepare index.html
      self.make_index( kws )
    end
      def make_index kws
       payload = "All data exchange by exchange. Data of " + Date.today.to_s + ":<br>"
         kws.each do |k|
           payload += " - <a href=\"./#{k}.json\">#{k}.json</a><br>"
         end
       file = @dbdump + 'index.html'
       inf = open( file, 'w+')
        inf.write( payload )
         inf.close
      end
    private
    def auth
      conf = YAML::load_file( File.join(__dir__, 'config.yaml') )['reddit']
      postres = `curl -X POST -d 'grant_type=password&username=#{conf['auth']['uname']}&password=#{conf['auth']['pass']}' -A "linux:sentiment v0.1.1 by /u/#{conf['auth']['uname']}" --user '#{conf['auth']['app']}:#{conf['auth']['secret']}' https://www.reddit.com/api/v1/access_token`
      authres = JSON.parse( postres )
      token = "bearer " + authres['access_token'].to_s
        p token
        return token
    end

  end



end#module Sentiment
