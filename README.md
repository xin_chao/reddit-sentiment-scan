## Reddit Sentiment

* Registered as _script_ app in Reddit API (must pass oauth)
* Scans subreddit for keywords (see `config.yaml`), stores matches in mongodb
* Data backup via firebase (only spec IP allowed to write)
* Runs daily (crontab), last run's data dumped into `index.html`

### Required gems

Ruby 2.2

* mongo
* firebase
* net/https

## To do

* bundler
* better dump
* db query (?)
* sentiment (?)

## See datasets

http://backtesting.bitcoinfutures.info/

or on firebase: https://bitex-sentiment.firebaseio.com/reddit.json
